package com.bluespurs.starterkit.controller;

import com.bluespurs.starterkit.UnitTest;
import com.bluespurs.starterkit.service.BestBuyApiService;
import com.bluespurs.starterkit.service.WalmartApiService;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Category(UnitTest.class)
public class ProductControllerUnitTest extends UnitTest {

    private MockMvc mockMvc;
    private WalmartApiService walmartApiService;
    private BestBuyApiService bestBuyApiService;

    @Before
    public void setUp() {
        super.setUp();
        walmartApiService = new WalmartApiService();
        bestBuyApiService = new BestBuyApiService();
        mockMvc = MockMvcBuilders.standaloneSetup(new ProductController(walmartApiService, bestBuyApiService)).build();
    }

    /**
     * Test the product search.
     *
     * @see ProductController#search(String)
     */
    @Test
    public void testSearch() throws Exception {
        mockMvc.perform(get("/product/search?name=apple")).andExpect(status().isOk());
        mockMvc.perform(get("/product/search?name=aappppllee")).andExpect(status().isNoContent());
    }
}
