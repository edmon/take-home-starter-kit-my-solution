package com.bluespurs.starterkit.controller;

import com.bluespurs.starterkit.service.BestBuyApiService;
import com.bluespurs.starterkit.service.WalmartApiService;
import com.bluespurs.starterkit.service.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final WalmartApiService walmartApiService;
    private final BestBuyApiService bestBuyApiService;

    @Autowired
    public ProductController(final WalmartApiService walmartApiService, final BestBuyApiService bestBuyApiService) {
        this.walmartApiService = walmartApiService;
        this.bestBuyApiService = bestBuyApiService;
    }

    /**
     * Returns the product of the lowest price.
     */
    @RequestMapping(value = "search", method = RequestMethod.GET, params = "name")
    public Object search(@RequestParam("name") final String name) {
        final List<Product> wps = walmartApiService.search(name, "price", true, 1);
        final List<Product> bbps = bestBuyApiService.search(name, "salePrice.asc", 1, 1);
        Product wp = null;
        Product bbp = null;
        if (!wps.isEmpty()) {
            wp = wps.get(0);
        }
        if (!bbps.isEmpty()) {
            bbp = bbps.get(0);
        }
        if (wp != null && (bbp == null || wp.getSalePrice() < bbp.getSalePrice())) {
            return new BestPriceProduct(wp.getName(), wp.getSalePrice(), "USD", "Walmart");
        }
        if (bbp != null) {
            return new BestPriceProduct(bbp.getName(), bbp.getSalePrice(), "USD", "BestBuy");
        }
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }
}
