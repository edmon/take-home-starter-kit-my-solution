package com.bluespurs.starterkit.controller;

public class BestPriceProduct {

    private final String productName;
    private final String bestPrice;
    private final String currency;
    private final String location;

    public BestPriceProduct(final String productName, final float bestPrice, final String currency,
                            final String location) {
        this.productName = productName;
        this.bestPrice = String.format("%.2f", bestPrice);
        this.currency = currency;
        this.location = location;
    }

    public String getProductName() {
        return productName;
    }

    public String getBestPrice() {
        return bestPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public String getLocation() {
        return location;
    }
}
